$ORIGIN media.avocode.com.
$TTL 1800
@       IN      SOA     gitiserver.com.      info@gitiserver.com (
                        2020061810        ; serial number
                        3600                    ; refresh
                        900                     ; retry
                        1209600                 ; expire
                        1800                    ; ttl
                        )
; Name servers
                    IN      NS      gitiserver.com.

; A records for name servers
gitiserver.             IN      A       95.217.80.28

; Additional A records
@               IN      A       95.217.80.28


